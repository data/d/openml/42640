# OpenML dataset: malicious_urls

https://www.openml.org/d/42640

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Mohammad Saiful Islam Mamun, Mohammad Ahmad Rathore, Arash Habibi Lashkari, Natalia Stakhanova and Ali A. Ghorbani
**Source**: [original](https://www.unb.ca/cic/datasets/url-2016.html) - Date unknown  
**Please cite**: Mohammad Saiful Islam Mamun, Mohammad Ahmad Rathore, Arash Habibi Lashkari, Natalia Stakhanova and Ali A. Ghorbani, &quot;Detecting Malicious URLs Using Lexical Analysis&quot;, Network and System Security, Springer International Publishing, P467--482, 2016.  

Rows with NaN and inf values removed. Data converted from CSV to ARFF.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42640) of an [OpenML dataset](https://www.openml.org/d/42640). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42640/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42640/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42640/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

